﻿using UnityEngine;
using System.Collections;

public class UIButtonSwitch : MonoBehaviour {

	public GameObject buttonsOn;
	public GameObject buttonsOff;
	
	// Update is called once per frame
	void Update () {
	
	}

	public void toButtons(){
		buttonsOn.SetActive (true);
		buttonsOff.SetActive (false);
	}
}
