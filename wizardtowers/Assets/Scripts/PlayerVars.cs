﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerVars : MonoBehaviour {

    public float health;
    public float magic;
    public float arcaneBombs;
    public float maxMagic;
    public float unitAmount;
    public bool recharging = false;
    public GameObject playerBase;
    public PhotonView networkPhotonView;

    private PhotonView thisView;
    private NetworkInGame manager;

    public Constants constants;
    public Text unitAmountText;

    void OnEnable()
    {
        thisView = GetComponent<PhotonView>();
        health = constants.towerStartHealth;
        magic = constants.magicStartAmount;
        arcaneBombs = constants.arcaneBombAmount;
        maxMagic = constants.maxMagic;
        unitAmount = 0;
        unitAmountText.text = "Units: " + unitAmount + " / " + constants.maxUnitAmount;
        networkPhotonView = GameObject.FindWithTag("NetworkManager").GetComponent<PhotonView>();
        manager = GameObject.FindWithTag("NetworkManager").GetComponent<NetworkInGame>();
    }

    void Start()
    {
        // Initial start of the recharge timer, if magic is not already at max.
        if (magic != maxMagic)
        {
            StartCoroutine("StartRechargeTimer");
        }
    }

    private IEnumerator StartRechargeTimer()
    {
        yield return new WaitForSeconds(constants.rechargeCooldown);
        StartCoroutine("RechargeMagic");
    }

    private IEnumerator RechargeMagic()
    {
        yield return new WaitForSeconds(constants.magicRechargeStep);
        calculateMagic(constants.magicRechargeRate);

        // Keep recharging if magic is not maxed.
        if (magic != maxMagic)
        {
            StartCoroutine("RechargeMagic");
        }
    }

    // Handles player tower taking damage
    [PunRPC]
    public void TakeDamage(float amount)
    {
        if (health > 0f)
        {
            health -= amount;
            health = Mathf.Clamp(health, 0f, constants.towerStartHealth);

            if (constants.offlineMode)
            {
                if (thisView.owner != null)
                {
                    networkPhotonView.RPC("UpdateHealthList_RPC", PhotonTargets.All, 1, health);
                }

                else
                {
                    networkPhotonView.RPC("UpdateHealthList_RPC", PhotonTargets.All, 2, health);
                }

                if (health == 0)
                {
                    if (playerBase.activeSelf)
                    {
                        playerBase.SetActive(false);
                        manager.EndGame(2);
                    }
                }
            }

            else
            {
                networkPhotonView.RPC("UpdateHealthList_RPC", PhotonTargets.All, thisView.owner.ID, health);

                if (health == 0 && thisView.isMine)
                {
                    if (playerBase.activeSelf)
                    {
                        playerBase.SetActive(false);
                        networkPhotonView.RPC("EndGame", PhotonTargets.All, thisView.owner.ID);
                    }
                }
            }
        }      
    }

    // Handles increasing/decreasing magic 
    [PunRPC]
    public void calculateMagic(float amount)
    {
        // Change magic value and make sure it stays within defined bounds.
        magic += amount;
        magic = Mathf.Clamp(magic, 0f, maxMagic);

        // Stop recharging for this player only if magic decreases.
        if (thisView.isMine && amount < 0)
        {
            StopAllCoroutines();
            StartCoroutine("StartRechargeTimer");
        }

        manager.UpdateMagicValue(magic);
    }

    public void changeUnitAmount(int amount)
    {
        unitAmount += amount;
        unitAmountText.text = "Units: " + unitAmount + " / " + constants.maxUnitAmount;
    }
}
