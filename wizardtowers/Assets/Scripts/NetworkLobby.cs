﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public class NetworkLobby : MonoBehaviour {

    public Constants constants;

    [SerializeField] Camera sceneCamera;
    [SerializeField] GameObject mainMenu;
    [SerializeField] GameObject lobbyWindow;
    [SerializeField] GameObject createRoomWindow;
    [SerializeField] GameObject joinPrivateWindow;
    [SerializeField] GameObject roomWindow;
    [SerializeField] GameObject wizardSelectionWindow;
    [SerializeField] GameObject errorWindow;
    [SerializeField] Text errorText;
    [SerializeField] ScrollRect roomList;
    [SerializeField] InputField newRoomName;
    [SerializeField] InputField privateRoomName;
    [SerializeField] Text roomNameText;
    [SerializeField] InputField playerName;
    [SerializeField] Text player1Name;
    [SerializeField] Text player2Name;
    [SerializeField] Image player1Wizard;
    [SerializeField] Image player2Wizard;
    [SerializeField] Toggle privateToggle;
    [SerializeField] Button startGameButton;
    [SerializeField] Text connectionText;
    [SerializeField] Text versionNumberText;

    public GameObject roomButton;
    public WizardSwitch wizardSwitch;
    public Texture2D cursorTexture;

    private Queue<string> messages;
    private const int messageCount = 6;
    private PhotonView photonView;

    // Use this for initialization
    void Awake()
    {
        versionNumberText.text = constants.versionNumber;
        photonView = GetComponent<PhotonView>();
        PhotonNetwork.logLevel = PhotonLogLevel.Full;
        //PhotonNetwork.autoCleanUpPlayerObjects = false;

        Cursor.SetCursor(cursorTexture, Vector2.zero, CursorMode.ForceSoftware);
    }

    private IEnumerator UpdateConnectionString()
    {
        while (true)
        {
            connectionText.text = PhotonNetwork.connectionStateDetailed.ToString();
            yield return null;
        }
    }

    public void StartTestMode()
    {
        PhotonNetwork.offlineMode = true;
        PhotonNetwork.player.name = "TestWizard";
        RoomOptions ro = new RoomOptions() { IsOpen = false, IsVisible = false, MaxPlayers = 1 };
        PhotonNetwork.CreateRoom("testroom", ro, TypedLobby.Default);
        PhotonNetwork.LoadLevel(1);
    }

    public void JoinLobby()
    {
        PhotonNetwork.offlineMode = false;
        PhotonNetwork.ConnectUsingSettings(constants.versionNumber);
        StartCoroutine("UpdateConnectionString");
        PhotonNetwork.autoJoinLobby = true;
        PhotonNetwork.automaticallySyncScene = true;
    }

    private void OnJoinedLobby()
    {
        lobbyWindow.SetActive(true);
        mainMenu.SetActive(false);
        StopCoroutine("UpdateConnectionString");
        connectionText.text = "";
    }

    private void OnReceivedRoomListUpdate()
    {
        foreach (Transform child in roomButton.transform.parent.transform)
        {
            if (child.gameObject.activeSelf)
            {
                Destroy(child.gameObject);
            }
        }

        RoomInfo[] rooms = PhotonNetwork.GetRoomList();

        foreach (RoomInfo room in rooms)
        {
            if (room.playerCount < room.maxPlayers)
            {
                GameObject roomBtn = Instantiate(roomButton);
                roomBtn.SetActive(true);
                roomBtn.transform.SetParent(roomButton.transform.parent, false);
                roomBtn.GetComponent<RoomButtonClick>().roomName = room.name;
                roomBtn.GetComponentInChildren<Text>().text = room.name + " " + room.playerCount + "/" + room.maxPlayers;
            }        
        }
    }

    // Buttons
    public void OpenCreateGameWindow()
    {
        lobbyWindow.SetActive(false);
        createRoomWindow.SetActive(true);
    }

    public void OpenJoinPrivateWindow()
    {
        lobbyWindow.SetActive(false);
        joinPrivateWindow.SetActive(true);
    }

    public void CreateRoom()
    {
        PhotonNetwork.player.name = playerName.text;
        RoomOptions ro = new RoomOptions() { IsOpen = true, IsVisible = !privateToggle.isOn, MaxPlayers = constants.maxPlayers };
        PhotonNetwork.CreateRoom(newRoomName.text, ro, TypedLobby.Default);
        createRoomWindow.SetActive(false);
    }

    public void JoinSelectedRoom(string roomName)
    {
        PhotonNetwork.player.name = playerName.text;
        PhotonNetwork.JoinRoom(roomName);
    }

    public void JoinPrivateRoom()
    {
        PhotonNetwork.player.name = playerName.text;
        PhotonNetwork.JoinRoom(privateRoomName.text);
    }

    public void JoinRandomRoom()
    {
        PhotonNetwork.player.name = playerName.text;
        PhotonNetwork.JoinRandomRoom();
    }

    public void LeaveRoom()
    {
        PhotonNetwork.LeaveRoom();
        roomWindow.SetActive(false);
        lobbyWindow.SetActive(true);
    }

    public void ReturnToMainMenu()
    {
        PhotonNetwork.LeaveLobby();
        PhotonNetwork.Disconnect();
        lobbyWindow.SetActive(false);
        mainMenu.SetActive(true);
    }

    public void ReturnFromErrorWindow()
    {
        if (PhotonNetwork.connected)
        {
            errorWindow.SetActive(false);
            joinPrivateWindow.SetActive(false);
            createRoomWindow.SetActive(false);
            lobbyWindow.SetActive(true);
        }

        else
        {
            errorWindow.SetActive(false);
        }
    }

    public void ExitGame()
    {
        if (PhotonNetwork.connected)
        {
            PhotonNetwork.Disconnect();
        }

        Application.Quit();
    }

    // Failures
    private void OnPhotonCreateRoomFailed()
    {
        errorText.text = "Failed to create room. Most likely a room with that name already exists. Please try again.";
        errorWindow.SetActive(true);
    }

    private void OnPhotonJoinRoomFailed()
    {
        errorText.text = "Failed to join selected room. No game with the selected name exists or the game already started. Please try again.";
        errorWindow.SetActive(true);
    }

    private void OnPhotonRandomJoinFailed()
    {
        errorText.text = "Failed to join random game. Perhaps there are no available games right now. Please try again or create a new game.";
        errorWindow.SetActive(true);
    }

    private void OnConnectionFail()
    {
        errorText.text = "Connection lost. Please check your internet connection and try again.";
        errorWindow.SetActive(true);
    }

    private void OnFailedToConnectToPhoton(DisconnectCause cause)
    {
        if (cause == DisconnectCause.MaxCcuReached || cause == DisconnectCause.DisconnectByServerUserLimit)
        {
            errorText.text = "Server is full. Please try again later.";
        }

        else if (cause == DisconnectCause.ExceptionOnConnect)
        {
            errorText.text = "Server is down at the moment, possibly due to maintenance. Please try again later.";
        }

        else
        {
            errorText.text = "Failed to connect to server. Please check your internet connection and try again";
        }

        errorWindow.SetActive(true);
        StopCoroutine("UpdateConnectionString");
        connectionText.text = "";
    }

    private void OnJoinedRoom()
    {
        if (PhotonNetwork.offlineMode == true)
        {
            return;
        }

        lobbyWindow.SetActive(false);
        roomWindow.SetActive(true);
        ToggleStartButton(false);

        PhotonNetwork.player.SetCustomProperties(new Hashtable() { { "selectedWizard", 0 } });

        if (PhotonNetwork.playerList.Length == 1)
        {
            if (PhotonNetwork.room.visible)
            {
                photonView.RPC("ChangeRoomText", PhotonTargets.AllBuffered, PhotonNetwork.room.name + " (Public)");
            }

            else
            {
                photonView.RPC("ChangeRoomText", PhotonTargets.AllBuffered, PhotonNetwork.room.name + " (Private)");
            }

            photonView.RPC("UpdatePlayerList", PhotonTargets.AllBuffered);
            //photonView.RPC("AddMessage", PhotonTargets.All, "<color=red>" + PhotonNetwork.player.name + "</color> <color=green>joined the game</color>");
        }

        else if (PhotonNetwork.playerList.Length == 2)
        {
            //photonView.RPC("AddMessage", PhotonTargets.All, "<color=blue>" + PhotonNetwork.player.name + "</color> <color=green>joined the game</color>");
        }
    }

    private void OnPhotonPlayerConnected()
    {
        photonView.RPC("UpdatePlayerList", PhotonTargets.All);

        if (PhotonNetwork.playerList.Length == PhotonNetwork.room.maxPlayers)
        {
            photonView.RPC("ToggleStartButton", PhotonTargets.All, true);
        }
    }

    private void OnPhotonPlayerDisconnected(PhotonPlayer leftPlayer)
    {
        photonView.RPC("ToggleStartButton", PhotonTargets.All, false);
        photonView.RPC("UpdatePlayerList", PhotonTargets.AllBuffered);
    }

    public void ClickWizard1Button()
    {
        if (PhotonNetwork.player.ID == 1)
        {
            wizardSelectionWindow.SetActive(true);
        }
    }

    public void ClickWizard2Button()
    {
        if (PhotonNetwork.player.ID == 2)
        {
            wizardSelectionWindow.SetActive(true);
        }
    }

    public void SelectWizard()
    {
        PhotonNetwork.player.SetCustomProperties(new Hashtable() { { "selectedWizard", wizardSwitch.selectedIndex } });
        photonView.RPC("UpdatePlayerList", PhotonTargets.AllBuffered);
        wizardSelectionWindow.SetActive(false);
    }

    public void StartGameClick()
    {
        photonView.RPC("StartGame", PhotonTargets.All);
    }

    [PunRPC]
    public void StartGame()
    {
        if (PhotonNetwork.player.isMasterClient)
        {
            PhotonNetwork.LoadLevel(1);
        }
    }

    [PunRPC]
    public void ToggleStartButton(bool toggled)
    {
        startGameButton.interactable = toggled;
    }

    [PunRPC]
    public void UpdatePlayerList()
    {
        player1Name.text = PhotonNetwork.masterClient.name;
        player1Wizard.sprite = wizardSwitch.wizardPictures[(int)PhotonNetwork.player.Get(1).customProperties["selectedWizard"]];

        if (PhotonNetwork.playerList.Length == 1)
        {
            player2Name.text = "<color=red>Waiting...</color>";
        }

        else
        {
            player2Name.text = PhotonNetwork.player.Get(2).name;
            player2Wizard.sprite = wizardSwitch.wizardPictures[(int)PhotonNetwork.player.Get(2).customProperties["selectedWizard"]];
        }  
    }

    [PunRPC]
    public void ChangeRoomText(string name)
    {
        roomNameText.text = name;
    }

    [PunRPC]
    public void AddMessage(string message)
    {
        messages.Enqueue(message);

        if (messages.Count > messageCount)
        {
            messages.Dequeue();
        }

        //messageWindow.text = "";

        foreach (string m in messages)
        {
            //messageWindow.text += m + "\n";
        }
    }
}
