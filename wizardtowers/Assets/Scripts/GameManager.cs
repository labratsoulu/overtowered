﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public Camera mainCamera;
    public Transform[] cameraPositions;
    public Constants constants;
    public GameObject[] Canvases;
    public NetworkInGame networkInGame;
    public GameObject menu;
    private PhotonView photonView;
    private bool UIToggled = true;
    private bool menuToggled = false;

    void Awake()
    {
        photonView = GetComponent<PhotonView>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            ToggleMenu();           
        }

        if (Input.GetKeyDown(KeyCode.F1))
        {
            if (constants.offlineMode)
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
            else
            {
                networkInGame.GetComponent<PhotonView>().RPC("Rematch", PhotonTargets.All);
            }         
        }

        if (Input.GetKeyDown(KeyCode.F2))
        {
            mainCamera.transform.position = cameraPositions[0].position;
            mainCamera.transform.rotation = cameraPositions[0].rotation;
        }

        if (Input.GetKeyDown(KeyCode.F3))
        {
            mainCamera.transform.position = cameraPositions[1].position;
            mainCamera.transform.rotation = cameraPositions[1].rotation;
        }

        if (Input.GetKeyDown(KeyCode.F4))
        {
            mainCamera.transform.position = cameraPositions[2].position;
            mainCamera.transform.rotation = cameraPositions[2].rotation;
        }

        if (Input.GetKeyDown(KeyCode.F5))
        {
            ToggleUI(UIToggled);
        }

        if (Input.GetKeyDown(KeyCode.F6))
        {
            if (constants.offlineMode)
            {
                GetComponent<CooldownReset>().ResetCooldown();
                GameObject.FindWithTag("Player1").GetComponent<PlayerVars>().arcaneBombs++;
                GameObject.FindWithTag("Player1").GetComponent<PlayerVars>().calculateMagic(100f);
            }
        }
    }

    public void ToggleMenu()
    {
        menuToggled = !menuToggled;
        menu.SetActive(menuToggled);    
    }

    public void ExitGame()
    {
        if (PhotonNetwork.connected)
        {
            PhotonNetwork.Disconnect();
        }

        Application.Quit();
    }

    private void ToggleUI(bool toggled)
    {
        foreach (GameObject go in Canvases)
        {
            go.SetActive(!toggled);                
        }

        UIToggled = !toggled;
    }
}
