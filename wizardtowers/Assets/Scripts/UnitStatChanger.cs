﻿using UnityEngine;
using System.Collections;

public class UnitStatChanger : MonoBehaviour {

	[HideInInspector]
	public float speedUpDuration = 0;
	[HideInInspector]
	public float damageUpDuration = 0;
	[HideInInspector]
	public float rangeUpDuration = 0;
	[HideInInspector]
	public float cDUpDuration = 0;
	[HideInInspector]
	public float speedDownDuration = 0;
	[HideInInspector]
	public float damageDownDuration = 0;
	[HideInInspector]
	public float rangeDownDuration = 0;
	[HideInInspector]
	public float cDDownDuration = 0;

	private Constants constants;
	//private float buffDuration = 15;

	private UnitAI ai;

	// Use this for initialization
	void Start () {
		ai = GetComponent<UnitAI> ();

		constants = GameObject.FindGameObjectWithTag ("Constants").GetComponent<Constants> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (speedUpDuration > 0) {
			speedUpDuration -= Time.deltaTime;
			if (speedUpDuration <= 0) {
				GetComponent<NavMeshAgent>().speed -= constants.spellOfHodosChange;
			}
		}
		if (damageUpDuration > 0) {
			damageUpDuration -= Time.deltaTime;
			if (damageUpDuration <= 0) {
				ai.damage -= constants.forLootAndGloryChange;
			}
		}
		if (rangeUpDuration > 0) {
			rangeUpDuration -= Time.deltaTime;
			if (rangeUpDuration <= 0) {
				ai.attackDistance -= constants.occultGlassesChange;
			}
		}
		if (cDUpDuration > 0) {
			cDUpDuration -= Time.deltaTime;
			if (cDUpDuration <= 0) {
				ai.attackInterval += constants.fatherTimesClockChange;
			}
		}

		if (speedDownDuration > 0) {
			speedDownDuration -= Time.deltaTime;
			if (speedDownDuration <= 0) {
				GetComponent<NavMeshAgent>().speed += constants.spellOfHodosChange;
			}
		}
		if (damageDownDuration > 0) {
			damageDownDuration -= Time.deltaTime;
			if (damageDownDuration <= 0) {
				ai.damage += constants.forLootAndGloryChange;
			}
		}
		if (rangeDownDuration > 0) {
			rangeDownDuration -= Time.deltaTime;
			if (rangeDownDuration <= 0) {
				ai.attackDistance += constants.occultGlassesChange;
			}
		}
		if (cDDownDuration > 0) {
			cDDownDuration -= Time.deltaTime;
			if (cDDownDuration <= 0) {
				ai.attackInterval -= constants.fatherTimesClockChange;
			}
		}
	}

	[PunRPC]
	public void changeSpeed(bool up)
	{
		if (up)
		{
			GetComponent<NavMeshAgent>().speed += constants.spellOfHodosChange;
			speedUpDuration = constants.buffDuration;
		}
		else
		{
			GetComponent<NavMeshAgent>().speed -= constants.spellOfHodosChange;
			speedDownDuration = constants.buffDuration;
		}
	}

	[PunRPC]
	public void changeRange(bool up)
	{
		if (up)
		{
			ai.attackDistance += constants.occultGlassesChange;
			rangeUpDuration = constants.buffDuration;
		}
		else
		{
			ai.attackDistance -= constants.occultGlassesChange;
			rangeDownDuration = constants.buffDuration;
		}
	}

	[PunRPC]
	public void changeDamage(bool up)
	{
		if (up)
		{
			ai.damage += constants.forLootAndGloryChange;
			damageUpDuration = constants.buffDuration;
		}
		else
		{
			ai.damage -= constants.forLootAndGloryChange;
			damageDownDuration = constants.buffDuration;
		}
	}

	[PunRPC]
	public void changeCooldown(bool up)
	{
		if (up)
		{
			ai.attackInterval -= constants.fatherTimesClockChange;
			cDUpDuration = constants.buffDuration;
		}
		else
		{
			ai.attackInterval += constants.fatherTimesClockChange;
			cDDownDuration = constants.buffDuration;
		}
	}
}
