﻿using UnityEngine;
using System.Collections;

public class ParticleSystemDisable : MonoBehaviour {

    void OnEnable()
    {
        StartCoroutine("DisableEffect");
    }

    private IEnumerator DisableEffect()
    {
        // Wait for the particle system to finish
        yield return new WaitForSeconds(GetComponent<ParticleSystem>().duration);

        // Disable particle system object
        gameObject.SetActive(false);
    }
}
