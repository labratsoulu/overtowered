﻿using UnityEngine;
using System.Collections;

public class TowerSounds : MonoBehaviour {

	[FMODUnity.EventRef]
	public string place = "event:/Gameplay/Tower Construction";


	public void placeSound(){
		FMODUnity.RuntimeManager.PlayOneShot (place, transform.position);
	}
}
