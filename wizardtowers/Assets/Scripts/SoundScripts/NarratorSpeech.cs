﻿using UnityEngine;
using System.Collections;

public class NarratorSpeech : MonoBehaviour {

	private FMOD.Studio.EventInstance disconnectSound;
	private FMOD.Studio.EventInstance victorySound;
	private FMOD.Studio.EventInstance defeatSound;
	private FMOD.Studio.EventInstance gameStartSound;
	private FMOD.Studio.EventInstance castSound;
	private FMOD.Studio.EventInstance arcaneBombCastSound;
	private FMOD.Studio.EventInstance noMagicSound;

	// Use this for initialization
	void OnEnable () {
		disconnectSound = FMODUnity.RuntimeManager.CreateInstance ("event:/Narrator/Game End/Player Disconnect/Old Man");
		victorySound = FMODUnity.RuntimeManager.CreateInstance ("event:/Narrator/Game End/Victory/Old Man");
		defeatSound = FMODUnity.RuntimeManager.CreateInstance ("event:/Narrator/Game End/Defeat/Old Man");
		gameStartSound = FMODUnity.RuntimeManager.CreateInstance ("event:/Narrator/Game Start/Old Man/Game Start");
		castSound = FMODUnity.RuntimeManager.CreateInstance ("event:/Narrator/UI Elements/Casting Spells/Old Man");
		arcaneBombCastSound = FMODUnity.RuntimeManager.CreateInstance ("event:/Narrator/UI Elements/Casting Spells/Arcane bomb/Old Man");
		noMagicSound = FMODUnity.RuntimeManager.CreateInstance ("event:/Narrator/UI Elements/No magic/Old Man");
	}

	public void disconnected(){
		disconnectSound.start ();
	}

	public void gameOver(bool victory){
		if (victory)
			victorySound.start ();
		else
			defeatSound.start ();
	}

	public void gameStart(){
		if(gameStartSound != null)
			gameStartSound.start ();
	}

	public void casting(){
		castSound.start ();
	}

	public void arcaneBomb(){
		arcaneBombCastSound.start ();
	}

	public void noMagic(){
		FMOD.Studio.PLAYBACK_STATE temp;
		noMagicSound.getPlaybackState (out temp);
		if (temp != FMOD.Studio.PLAYBACK_STATE.PLAYING)
			noMagicSound.start ();
	}
}
