﻿using UnityEngine;
using System.Collections;

public class UnitSounds : MonoBehaviour {

	[FMODUnity.EventRef]
	public string attack;

	[FMODUnity.EventRef]
	public string death;

	public void attackSound(){
		FMODUnity.RuntimeManager.PlayOneShot (attack, transform.position);
	}

	public void deathSound(){
		FMODUnity.RuntimeManager.PlayOneShot (death, transform.position);
	}
}
