﻿using UnityEngine;
using System.Collections;

public class UnitAI : MonoBehaviour {
   
    public GameObject initialTarget;
    public GameObject currentTarget;
    public LayerMask targetLayer;

    public enum unitTypes { Golem, Apprentice, Skeleton };
    public unitTypes unitType;

    public float health;
    public float damage;
    public float attackInterval;
    public float movementSpeed;
    public float attackDistance;
    public float magicReward;

    public float targetCheckInterval;
    public float targetAcquireDistance;

    public Collider[] enemyColliders;

    // Enum to check current unit state
    public enum unitStates { Moving, Attacking, Dead };
    public unitStates unitState;

	public UnitSounds soundScript;
    public GameObject projectile;
    public GameObject projectileStartLocation;

    public GameObject[] unitEffects;

    private Animator anim;
    private PhotonView thisView;
    private NavMeshAgent agent;
    private Constants constants;
    private GameObject player1Vars;
    private GameObject player2Vars;
    private float distance;
    private bool isStunned = false;
    private float stunDuration = 0;
    private bool isDying = false;

    // Make sure constants are loaded first
    void Awake()
    {
        constants = GameObject.FindWithTag("Constants").GetComponent<Constants>();
        player1Vars = GameObject.FindWithTag("Player1");
        player2Vars = GameObject.FindWithTag("Player2");

        switch (unitType)
        {
		    case unitTypes.Golem:
				health = constants.golemHealth;
				damage = constants.golemDamage;
				attackInterval = constants.golemAttackInterval;
				movementSpeed = constants.golemMovementSpeed;
				attackDistance = constants.golemAttackDistance;
				magicReward = constants.golemMagicReward;
                break;

            case unitTypes.Apprentice:
                health = constants.apprenticeHealth;
                damage = constants.apprenticeDamage;
                attackInterval = constants.apprenticeAttackInterval;
                movementSpeed = constants.apprenticeMovementSpeed;
                attackDistance = constants.apprenticeAttackDistance;
                magicReward = constants.apprenticeMagicReward;
                break;

            case unitTypes.Skeleton:
                health = constants.skeletonHealth;
                damage = constants.skeletonDamage;
                attackInterval = constants.skeletonAttackInterval;
                movementSpeed = constants.skeletonMovementSpeed;
                attackDistance = constants.skeletonAttackDistance;
                magicReward = constants.skeletonMagicReward;
                break;
        }

        targetCheckInterval = constants.targetCheckInterval;
        targetAcquireDistance = constants.targetAcquireDistance;  
    }

    void Start()
    {
        thisView = GetComponent<PhotonView>();
        agent = GetComponent<NavMeshAgent>();
        agent.stoppingDistance = attackDistance;
        agent.speed = movementSpeed;
        anim = GetComponent<Animator>();

        // Player1 targets Player 2 tower and vice versa
        if (gameObject.layer == 9) // = Player1
        {
            initialTarget = GameObject.FindWithTag("Player2");
        }

        else if (gameObject.layer == 10) // = Player2
        {
            initialTarget = GameObject.FindWithTag("Player1");
        }

        // Start coroutine for checking targets
        StartCoroutine("SelectTarget");
        unitState = unitStates.Moving;

        currentTarget = initialTarget;
        agent.destination = currentTarget.transform.position;
    }

    void Update ()
    {
        //print("rem. dist: " + agent.remainingDistance);
        //print("pending: " + agent.pathPending);
        //print("pathstatus: " + agent.pathStatus);
        //print("haspath:" + agent.hasPath);
        anim.SetFloat("movementSpeedMultiplier", agent.speed / 3);
        anim.SetFloat("attackSpeedMultiplier", 1 / attackInterval);

        if (isStunned)
        {
            if (stunDuration > 0)
            {
                stunDuration -= Time.deltaTime;
                agent.Stop();
                anim.enabled = false;
            }

            else
            {
                agent.Resume();
                anim.enabled = true;
                isStunned = false;
            }
        }

        else
        {
            // Check if we have arrived at our target. If true, stop moving and start attacking
            if (agent.enabled)
            {
                distance = Vector3.Distance(transform.position, currentTarget.transform.position);

                if (!agent.pathPending && distance <= attackDistance)
                {
                    anim.SetBool("isMoving", false);
                    anim.SetBool("isAttacking", true);
                    unitState = unitStates.Attacking;
                }

                // Otherwise, keep moving
                else
                {
                    anim.SetBool("isMoving", true);
                    anim.SetBool("isAttacking", false);
                    unitState = unitStates.Moving;
                }
            }

            // Smooth facing towards the target if attacking
            if (unitState == unitStates.Attacking)
            {
                if (currentTarget != null)
                {
                    Vector3 targetPos = currentTarget.transform.position - transform.position;
                    Quaternion newRot = Quaternion.LookRotation(targetPos);
                    transform.rotation = Quaternion.Lerp(transform.rotation, newRot, Time.deltaTime * 4);
                }
            }
        }        
    }

    // Function to select target. The target needs to be in <targetLayer>, otherwise ignored
    private IEnumerator SelectTarget()
    {   
        if (currentTarget = null)
        {
            currentTarget = initialTarget;
        }

        if (currentTarget = initialTarget)
        {
            enemyColliders = Physics.OverlapSphere(gameObject.transform.position, targetAcquireDistance, targetLayer);

            if (enemyColliders.Length != 0)
            {
                // This should sort the array in order from the closest to the furthest one
                System.Array.Sort(enemyColliders, delegate (Collider a, Collider b) {
                    return (transform.position - a.transform.position).sqrMagnitude.CompareTo((transform.position - b.transform.position).sqrMagnitude);
                });

                currentTarget = enemyColliders[0].gameObject;
            }
        }
        
        else
        {
            if (currentTarget.GetComponent<UnitAI>().unitState == unitStates.Dead)
            {
                currentTarget = initialTarget;
            }
        }               

        agent.destination = currentTarget.transform.position;

        yield return new WaitForSeconds(targetCheckInterval);
        StartCoroutine("SelectTarget");
    }

    // This is called in Golem attack animation.
    public void DoDamage()
    {
        if (currentTarget != null && currentTarget.activeSelf && PhotonNetwork.isMasterClient)
        {
			soundScript.attackSound();
            currentTarget.GetComponent<PhotonView>().RPC("TakeDamage", PhotonTargets.All, damage);
        }
    }

    // This is called in Skeleton and Cultist attack animation. Their projectiles do the damage on hit.
    public void ShootProjectile()
    {
        if (currentTarget != null && !agent.pathPending && distance <= attackDistance)
        {
			soundScript.attackSound();
            GameObject proj = (GameObject)Instantiate(projectile, projectileStartLocation.transform.position, transform.rotation);
            proj.SetActive(true);
            proj.GetComponent<Projectile>().damage = damage;
            proj.GetComponent<Projectile>().target = currentTarget;
        }     
    }

    [PunRPC]
    public void TakeDamage(float amount)
    {
        
        health -= amount;

        if (health <= 0)
        {
            // This option can be toggled on/off in constants.
            if (constants.giveMagicOnKill)
            {
                // Give magic to the killer (NOT the owner of this unit)
                if (!thisView.isMine)
                {
                    if (thisView.owner.ID == 1)
                    {
                        player2Vars.GetComponent<PhotonView>().RPC("calculateMagic", PhotonTargets.All, magicReward);
                    }
                    else // ID == 2
                    {
                        player1Vars.GetComponent<PhotonView>().RPC("calculateMagic", PhotonTargets.All, magicReward);
                    }
                }
            }

            health = 0;
            StopAllCoroutines();
            anim.SetBool("isMoving", false);
            anim.SetBool("isAttacking", false);
            anim.SetBool("isDead", true);
            unitState = unitStates.Dead;
            agent.enabled = false;
            gameObject.layer = 1;

			soundScript.deathSound ();

            foreach (GameObject effect in unitEffects)
            {
                effect.SetActive(false);
            }

            //Owner of this unit destroys it for everyone
            if (thisView.isMine && !isDying)
            {
                isDying = true;

                if (!constants.testMode)
                {
                    if (thisView.owner.ID == 1)
                    {
                        player1Vars.GetComponent<PlayerVars>().changeUnitAmount(-1);
                    }
                    else // ID == 2
                    {
                        player2Vars.GetComponent<PlayerVars>().changeUnitAmount(-1);
                    }
                }          
            }
        }   
    }

    public void DestroyUnit()
    {
        if (thisView.isMine)
        {
            PhotonNetwork.Destroy(gameObject);
        }      
    }

	//Heals unit, but never lets health go beyond initial max
	public void heal(float amount)
    {
		switch (unitType)
		{
			case unitTypes.Golem:
				if (health + amount >= constants.golemHealth)
					health = constants.golemHealth;
				else
					health += amount;
				break;

			case unitTypes.Apprentice:
				if (health + amount >= constants.apprenticeHealth)
					health = constants.golemHealth;
				else
					health += amount;
				break;

			case unitTypes.Skeleton:
				if (health + amount >= constants.skeletonHealth)
					health = constants.golemHealth;
				else
					health += amount;
				break;
		}
	}

	//stun this unit for a given amount of seconds
	public void stun(float seconds)
    {
        isStunned = true;
		stunDuration = seconds;
	}

    // Activate spell particle effect for all players
    [PunRPC]
    public void ActivateEffect(int effectNumber)
    {
        unitEffects[effectNumber].SetActive(true);
    }
}
