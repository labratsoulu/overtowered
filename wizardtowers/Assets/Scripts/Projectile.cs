﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {

    public GameObject startEffect;
    public ParticleSystem projectileEffect;
    public ParticleSystem hitEffect;
    public GameObject projectileObject;
    public GameObject target;
    public enum projectileTypes { Skeleton, Cultist };
    public projectileTypes projectileType;
    public float damage;
    public float projectileSpeed;
    public float arcHeight;

    private bool isMoving = true;
    private Vector3 startPos;

    void Start()
    {
        if (startEffect != null)
        {
            GameObject effect = (GameObject)Instantiate(startEffect, transform.position, transform.rotation);
            effect.SetActive(true);
        }

        projectileEffect.Play(false);
        startPos = transform.position;
    }

	void Update ()
    {
        if (isMoving)
        {
            float step = projectileSpeed * Time.deltaTime;

            if (projectileType == projectileTypes.Skeleton)
            {
                Vector2 xz0 = new Vector2(startPos.x, startPos.z);
                Vector2 xz1 = new Vector2(target.transform.position.x, target.transform.position.z);
                float dist = Vector2.Distance(xz1, xz0);
                Vector2 nextXZ = Vector2.MoveTowards(new Vector2(transform.position.x, transform.position.z), xz1, step);
                float baseY = Mathf.Lerp(startPos.y, target.transform.position.y + 2.5f, Vector2.Distance(nextXZ, xz0) / dist);
                float arc = arcHeight * Vector2.Distance(nextXZ, xz0) * Vector2.Distance(nextXZ, xz1) / (0.25f * dist * dist);
                Vector3 nextPos = new Vector3(nextXZ.x, baseY + arc, nextXZ.y);
                transform.position = nextPos;
            }

            else //projectileType == projectileTypes.Cultist
            {
                transform.position = Vector3.MoveTowards(transform.position, target.transform.position + new Vector3(0, 2.5f, 0), step);
            }          
        }      
	}

    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject == target)
        {
            if (projectileObject != null)
            {
                projectileObject.SetActive(false);
            }

            isMoving = false;
            GetComponent<Light>().enabled = false;
            projectileEffect.Stop(false);
            hitEffect.Play();
            Destroy(gameObject, hitEffect.duration);

            if (target != null && target.activeSelf && PhotonNetwork.isMasterClient)
            {
                target.GetComponent<PhotonView>().RPC("TakeDamage", PhotonTargets.All, damage);
            }
        }
    }
}
