﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class ActiveElementClear : MonoBehaviour {

    public void ClearElement()
    {
        EventSystem.current.SetSelectedGameObject(null);
    }
 
}
