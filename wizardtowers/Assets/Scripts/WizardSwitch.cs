﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WizardSwitch : MonoBehaviour {

    public Text wizardNameText;
    public Text wizardDescriptionText;
    public Image wizardImage;

    public string[] wizardNames;

    [TextArea(5, 10)]
    public string[] wizardDescriptions;

    public Sprite[] wizardPictures;

    public int selectedIndex = 0;
	
	public void PreviousWizard()
    {
        selectedIndex--;

        if (selectedIndex == -1)
        {
            selectedIndex = wizardNames.Length - 1;
        }

        wizardNameText.text = wizardNames[selectedIndex];
        wizardDescriptionText.text = wizardDescriptions[selectedIndex];
        wizardImage.sprite = wizardPictures[selectedIndex];
	}

    public void NextWizard()
    {
        selectedIndex++;

        if (selectedIndex == wizardNames.Length)
        {
            selectedIndex = 0;
        }

        wizardNameText.text = wizardNames[selectedIndex];
        wizardDescriptionText.text = wizardDescriptions[selectedIndex];
        wizardImage.sprite = wizardPictures[selectedIndex];
    }
 
}