﻿using UnityEngine;
using System.Collections;

public class Constants : MonoBehaviour {

    [Header("Game Settings")]

    [Tooltip("Build version. Only players with same version can join the same game.")]
    public string versionNumber;


    [Header("Player Variables")]

    [Tooltip("Number of players required to start the game. Set this to 1 to test single player, 2 for multiplayer")]
    public int playersToStart;

    [Tooltip("Maximum number of players in a room")]
    public byte maxPlayers;

    [Tooltip("Starting health of the towers")]
    public float towerStartHealth;

    [Tooltip("Amount of magic at the start (per player)")]
    public float magicStartAmount;

    [Tooltip("Maximum amount of mana (per magic bar)")]
    public float maxMagic;

    [Tooltip("Amount of magic each tower piece costs")]
    public float pieceMagicCost;

    [Tooltip("Amount of Arcane Bombs per game")]
    public float arcaneBombAmount;

    [Tooltip("Cooldown after spending magic after which magic starts recharging again")]
    public float rechargeCooldown;

    [Tooltip("Amount of magic recovered every step when recharging (magic increases by *rate* every *step* seconds)")]
    public float magicRechargeRate;

    [Tooltip("The interval of magic recharge (magic increases by *rate* every *step* seconds)")]
    public float magicRechargeStep;

    [Tooltip("The maximum number of units one player can have at any given time)")]
    public float maxUnitAmount;

    [Tooltip("Whether or not killing units gives magic to the killer")]
    public bool giveMagicOnKill;

    [Tooltip("Offline mode on/off for testing")]
    public bool offlineMode;

    [Tooltip("Test mode on/off, disables unit limits")]
    public bool testMode;

    [Tooltip("Lock camera X axis on/off")]
    public bool cameraXLock;

    [Tooltip("If X lock is used, rotation of X axis in degrees")]
    public float cameraXRotation;


    [Header("Unit Specific Variables")]

    [Tooltip("Starting and max health of the unit")]
    public float golemHealth;

    [Tooltip("Damage of the unit")]
    public float golemDamage;

    [Tooltip("Attack speed of the unit (Unit attacks every *interval* seconds)")]
    public float golemAttackInterval;

    [Tooltip("Movement speed of the unit")]
    public float golemMovementSpeed;

    [Tooltip("Attack distance of the unit")]
    public float golemAttackDistance;

    [Tooltip("Amount of magic the other player is given when killing this unit")]
    public float golemMagicReward;

    [Space(5)]

    [Tooltip("Starting and max health of the unit")]
    public float apprenticeHealth;

    [Tooltip("Damage of the unit")]
    public float apprenticeDamage;

    [Tooltip("Attack speed of the unit (Unit attacks every *interval* seconds)")]
    public float apprenticeAttackInterval;

    [Tooltip("Movement speed of the unit")]
    public float apprenticeMovementSpeed;

    [Tooltip("Attack distance of the unit")]
    public float apprenticeAttackDistance;

    [Tooltip("Amount of magic the other player is given when killing this unit")]
    public float apprenticeMagicReward;

    [Space(5)]

    [Tooltip("Starting and max health of the unit")]
    public float skeletonHealth;

    [Tooltip("Damage of the unit")]
    public float skeletonDamage;

    [Tooltip("Attack speed of the unit (Unit attacks every *interval* seconds)")]
    public float skeletonAttackInterval;

    [Tooltip("Movement speed of the unit")]
    public float skeletonMovementSpeed;

    [Tooltip("Attack distance of the unit")]
    public float skeletonAttackDistance;

    [Tooltip("Amount of magic the other player is given when killing this unit")]
    public float skeletonMagicReward;


    [Header("Global Unit Variables")]

    [Tooltip("How often the unit checks for targets around it (every *interval* seconds). Dont't set this too low.")]
    public float targetCheckInterval; // 0.5

    [Tooltip("How far the unit acquires targets")]
    public float targetAcquireDistance; //30

    [Tooltip("Unit spawns every *rate* seconds")]
    public float unitSpawnRate; // 10

    [Tooltip("Unit spawn area radius")]
    public float unitSpawnRadius;


	[Header("Spell Variables")]

	[Tooltip("The magic cost of Spell of Hodos")]
	public float spellOfHodosCost = 75;

	[Tooltip("The cooldown time of Spell of Hodos")]
	public float spellOfHodosCD = 75;

	[Tooltip("Spell of Hodos speed change")]
	public float spellOfHodosChange = 5;

	[Tooltip("The magic cost of Father Time's Clock")]
	public float fatherTimesClockCost = 75;

	[Tooltip("The cooldown time of Father Time's Clock")]
	public float fatherTimesClockCD = 75;

	[Tooltip("Father Time's Clock Cooldown change")]
	public float fatherTimesClockChange = 0.5f;

	[Tooltip("The magic cost of Occult Glasses")]
	public float occultGlassesCost = 100;

	[Tooltip("The cooldown time of Occult Glasses")]
	public float occultGlassesCD = 100;

	[Tooltip("Occult Glasses range change")]
	public float occultGlassesChange = 5;

	[Tooltip("The magic cost of For Loot And Glory")]
	public float forLootAndGloryCost = 75;

	[Tooltip("The cooldown time of For Loot And Glory")]
	public float forLootAndGloryCD = 75;

	[Tooltip("For Loot and Glory damage change")]
	public float forLootAndGloryChange = 10;

	[Tooltip("The magic cost of Divine Intervention")]
	public float divineInterventionCost = 100;

	[Tooltip("The cooldown time of Divine Intervention")]
	public float divineInterventionCD = 100;

	[Tooltip("The heal amount of Divine Intervention")]
	public float divineInterventionHeal = 50;

	[Tooltip("The stun duration of Divine Intervention")]
	public float divineInterventionStunDuration = 3;

	[Tooltip("Buff duration")]
	public float buffDuration = 15;

	[Tooltip("Spell Radius")]
	public float spellRadius = 25;
}
