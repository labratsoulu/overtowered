﻿using UnityEngine;
using System.Collections;

public class NetworkTest : MonoBehaviour {

	void Start () {
        PhotonNetwork.offlineMode = true;
        PhotonNetwork.player.name = "TestWizard";
        RoomOptions ro = new RoomOptions() { IsOpen = false, IsVisible = false, MaxPlayers = 1 };
        PhotonNetwork.CreateRoom("testroom", ro, TypedLobby.Default);
    }
}
