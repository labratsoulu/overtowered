﻿using UnityEngine;
using System.Collections;

public class temptest : MonoBehaviour {

	GameObject selectedPiece;

	public GameObject MainPiece;
	public GameObject ConnectorPiece;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		
		if (Input.GetMouseButtonDown (0)) {
			selectedPiece = Instantiate (MainPiece);
			selectedPiece.GetComponent<MainPiece> ().StartDrag ();

		} else if (Input.GetMouseButtonDown (1)) {
			selectedPiece = Instantiate (ConnectorPiece);
			selectedPiece.GetComponent<ConnectorPiece> ().StartDrag ();
		}
	}
}
