﻿using UnityEngine;
using System.Collections;

public class TowerPieceSpawner : MonoBehaviour {

	public GameObject blueGolemPiece;
	public GameObject blueCultistPiece;
	public GameObject blueSkeletonPiece;
	public GameObject blueConnectorPiece;
	public GameObject redGolemPiece;
	public GameObject redCultistPiece;
	public GameObject redSkeletonPiece;
    public GameObject redConnectorPiece;

    public Constants constants;
    public PlayerVars player1Vars;
    public PlayerVars player2Vars;

    private GameObject piece;
    private float totalMagic;

	public GameObject noMagicText;

    GameObject selectedPiece;

	public void spawnPiece(string unitType){

		if (PhotonNetwork.player.ID == 1)
		{
			totalMagic = player1Vars.magic;
		}

		else // ID == 2
		{
			totalMagic = player2Vars.magic;
		}

		if (totalMagic >= constants.pieceMagicCost) { 
			switch (unitType) {
			case "golem":
				if (PhotonNetwork.player.ID == 1) {
					piece = redGolemPiece;
				} else { // ID == 2
					piece = blueGolemPiece;
				}
				break;
			case "apprentice":
				if (PhotonNetwork.player.ID == 1) {
					piece = redCultistPiece;
				} else { // ID == 2
					piece = blueCultistPiece;
				}
				break;
			case "skeleton":
				if (PhotonNetwork.player.ID == 1) {
					piece = redSkeletonPiece;
				} else { // ID == 2
					piece = blueSkeletonPiece;
				}
				break;
			case "damage":
				if (PhotonNetwork.player.ID == 1) {
					//piece = redMainPiece;
				} else { // ID == 2
					//piece = blueMainPiece;
				}
				break;
			case "health":
				if (PhotonNetwork.player.ID == 1) {
					//piece = redMainPiece;
				} else { // ID == 2
					//piece = blueMainPiece;
				}
				break;
			case "range":
				if (PhotonNetwork.player.ID == 1) {
					//piece = redMainPiece;
				} else { // ID == 2
					//piece = blueMainPiece;
				}
				break;
			case "cooldown":
				if (PhotonNetwork.player.ID == 1) {
					//piece = redMainPiece;
				} else { // ID == 2
					//piece = blueMainPiece;
				}
				break;
			}
			selectedPiece = Instantiate (piece);
			selectedPiece.GetComponent<MainPiece> ().StartDrag ();
		} else {
			noMagicText.SetActive (true);
			noMagicText.GetComponent<NoMagicText> ().showText ();
			GameObject.FindGameObjectWithTag ("narrator").GetComponent<NarratorSpeech> ().noMagic ();
		}
	}

	public void spawnConnector(){

        if (PhotonNetwork.player.ID == 1)
        {
            piece = redConnectorPiece;
        }

        else
        {
            piece = blueConnectorPiece;
        }

        selectedPiece = Instantiate(piece);
        selectedPiece.GetComponent<ConnectorPiece>().StartDrag();
    }
}
