﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ArcaneBomb : MonoBehaviour {

	protected RaycastHit hit;

	private LayerMask unitLayer;
	private float damage = 9999f;
    private Constants constants;
    private PlayerVars playerVars;
	private string spellsound = "event:/Spells/AoE";

	void Awake()
    {
        //constants = GameObject.FindWithTag("Constants").GetComponent<Constants>();
		if (PhotonNetwork.player.ID == 1)
        {
			//blue player
			unitLayer = LayerMask.NameToLayer("Player2");
            playerVars = GameObject.FindWithTag("Player1").GetComponent<PlayerVars>();
		}
        else // playerID == 2
        {
			unitLayer = LayerMask.NameToLayer("Player1");
            playerVars = GameObject.FindWithTag("Player2").GetComponent<PlayerVars>();
        }

        constants = GameObject.FindWithTag("Constants").GetComponent<Constants>();
    }
	
	// Update is called once per frame
	void Update()
    {
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		if (Physics.Raycast(ray, out hit))
		{
			transform.position = new Vector3(hit.point.x, hit.point.y, hit.point.z);
		}
		EndDrag();
	}

	protected void EndDrag()
	{
		if (Input.GetMouseButtonDown (0)) {
			GameObject.FindGameObjectWithTag ("narrator").GetComponent<NarratorSpeech> ().arcaneBomb ();
			FMODUnity.RuntimeManager.PlayOneShot (spellsound, transform.position);
            GameObject.FindGameObjectWithTag("arcaneBomb").GetComponent<Button>().interactable = false;
			if (hit.transform != null) {
                // This is the bomb effect, detonating some distance up from target position
                PhotonNetwork.Instantiate("ArcaneBombEffect", transform.position + new Vector3(0, 2.5f, 0), Quaternion.Euler(-90, 0, 0), 0);
				Collider[] hitColliders = Physics.OverlapSphere (transform.position, constants.spellRadius);
				foreach (Collider unit in hitColliders) {
					if (unit.gameObject.layer == unitLayer) {
						unit.gameObject.GetComponent<PhotonView> ().RPC ("TakeDamage", PhotonTargets.All, damage);
					}
				}
                playerVars.arcaneBombs--;
			}
			Destroy(gameObject);
		} else if (Input.GetMouseButtonDown(1)){
			Destroy(gameObject);
		}
	}
}
