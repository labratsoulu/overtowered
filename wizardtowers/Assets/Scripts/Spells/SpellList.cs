﻿using UnityEngine;
using System.Collections;

public class SpellList : MonoBehaviour {

    public PlayerVars player1Vars;
    public PlayerVars player2Vars;
    private PlayerVars playerVars;

    public GameObject ArcaneBomb;
	public GameObject SpellOfHodos;
	public GameObject FatherTimesClock;
	public GameObject OccultGlasses;
	public GameObject ForLootAndGlory;
	public GameObject DivineIntervention;

	public GameObject noMagicText;

	public Constants constants;

	void Awake(){
		if (PhotonNetwork.player.ID == 1)
		{
			playerVars = player1Vars;
		}

		else // ID == 2
		{
			playerVars = player2Vars;
		}

	}

	public void arcaneBomb(){
        if (playerVars.arcaneBombs != 0)
        {
            Instantiate(ArcaneBomb);
        }	
	}

	public void spellOfHodos(){
		if(playerVars.magic >= constants.spellOfHodosCost)
			Instantiate (SpellOfHodos);
		else{
			//not enough magic
			noMagic();
		}
	}

	public void fatherTimesClock(){
		if(playerVars.magic >= constants.fatherTimesClockCost)
			Instantiate (FatherTimesClock);
		else{
			//not enough magic
			noMagic();
		}
	}

	public void occultGlasses(){
		if(playerVars.magic >= constants.occultGlassesCost)
			Instantiate (OccultGlasses);
		else{
			//not enough magic
			noMagic();
		}
	}

	public void forLootAndGlory(){
		if(playerVars.magic >= constants.forLootAndGloryCost)
			Instantiate (ForLootAndGlory);
		else{
			//not enough magic
			noMagic();
		}
	}

	public void divineIntervention(){
		if(playerVars.magic >= constants.divineInterventionCost)
			Instantiate (DivineIntervention);
		else{
			//not enough magic
			noMagic();
		}
	}

	public void noMagic(){
		noMagicText.SetActive (true);
		noMagicText.GetComponent<NoMagicText> ().showText ();
		GameObject.FindGameObjectWithTag ("narrator").GetComponent<NarratorSpeech> ().noMagic ();
	}
}
