﻿using UnityEngine;
using System.Collections;

public class StatChange : MonoBehaviour {
	
	protected RaycastHit hit;

	private float spellCost;
	private float cooldownTime;
	public statToChange statChange;

	public enum statToChange{
        changeCooldown,
        changeDamage,
        changeRange,
        changeSpeed
	};

	private Constants constants;

	//private float spellRadius = 25f;
	private LayerMask unitLayer;
	private LayerMask opponentLayer;
	private PlayerVars playerVars;

	private string spellsound;

	void Awake ()
	{
        print((int)statChange + " ast");
		//constants = GameObject.FindWithTag("Constants").GetComponent<Constants>();
		if (PhotonNetwork.player.ID == 1)
		{
			//blue player
			unitLayer = LayerMask.NameToLayer("Player1");
			opponentLayer = LayerMask.NameToLayer("Player2");
			playerVars = GameObject.FindWithTag("Player1").GetComponent<PlayerVars>();
		}
		else // playerID == 2
		{
			unitLayer = LayerMask.NameToLayer("Player2");
			opponentLayer = LayerMask.NameToLayer("Player1");
			playerVars = GameObject.FindWithTag("Player2").GetComponent<PlayerVars>();
		}

		constants = GameObject.FindGameObjectWithTag("Constants").GetComponent<Constants>();

		switch(statChange){
		case statToChange.changeCooldown:
			spellCost = constants.fatherTimesClockCost;
			cooldownTime = constants.fatherTimesClockCD;
			spellsound = "event:/Spells/Speed attack";
			break;

		case statToChange.changeDamage:
			spellCost = constants.forLootAndGloryCost;
			cooldownTime = constants.forLootAndGloryCD;
			spellsound = "event:/Spells/Damage up";
			break;

		case statToChange.changeRange:
			spellCost = constants.occultGlassesCost;
			cooldownTime = constants.occultGlassesCD;
			spellsound = "event:/Spells/Range up";
			break;

		case statToChange.changeSpeed:
			spellCost = constants.spellOfHodosCost;
			cooldownTime = constants.spellOfHodosCD;
			spellsound = "event:/Spells/Speeding unit";
			break;
		}
	}

	// Update is called once per frame
	void Update ()
	{
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		if (Physics.Raycast(ray, out hit))
		{
			transform.position = new Vector3(hit.point.x, hit.point.y, hit.point.z);
		}
		EndDrag();
	}

	protected void EndDrag()
	{
		if (Input.GetMouseButtonDown (0)) {
			GameObject.FindGameObjectWithTag ("narrator").GetComponent<NarratorSpeech> ().casting ();
			FMODUnity.RuntimeManager.PlayOneShot (spellsound, transform.position);
			playerVars.calculateMagic(-spellCost);
			GameObject.FindGameObjectWithTag (statChange.ToString ()).GetComponent<SpellButtonCooldown> ().putOnCooldown (cooldownTime);
			if (hit.transform != null) {
				// This is the bomb effect, detonating some distance up from target position
				PhotonNetwork.Instantiate(statChange.ToString(), transform.position + new Vector3(0, 2.5f, 0), Quaternion.Euler(-90, 0, 0), 0);
                Collider[] hitColliders = Physics.OverlapSphere (transform.position, constants.spellRadius);
				foreach (Collider unit in hitColliders) {
					if (unit.gameObject.layer == unitLayer) {
						unit.gameObject.GetComponent<PhotonView>().RPC (statChange.ToString(), PhotonTargets.All, true);
                        unit.gameObject.GetComponent<PhotonView>().RPC("ActivateEffect", PhotonTargets.All, (int)statChange);
					} else if (unit.gameObject.layer == opponentLayer) {
						unit.gameObject.GetComponent<PhotonView>().RPC (statChange.ToString(), PhotonTargets.All, false);
                        unit.gameObject.GetComponent<PhotonView>().RPC("ActivateEffect", PhotonTargets.All, (int)statChange);
                    }
				}
			}
			Destroy(gameObject);
		} else if (Input.GetMouseButtonDown(1)){
			Destroy(gameObject); 
		}
	}
}
