﻿using UnityEngine;
using System.Collections;

public class DivineIntervention : MonoBehaviour {

	protected RaycastHit hit;

	private Constants constants;
	
	//public float spellCost;
	//public float cooldownTime;

	//private float spellRadius = 25f;
	//private float healAmount = 50f;
	//private float stunDuration = 3f;
	private LayerMask unitLayer;
	private LayerMask opponentLayer;
	private PlayerVars playerVars;

	private string spellsound = "event:/Spells/Healing";

	void Awake ()
	{
		//constants = GameObject.FindWithTag("Constants").GetComponent<Constants>();
		if (PhotonNetwork.player.ID == 1)
		{
			//blue player
			unitLayer = LayerMask.NameToLayer("Player1");
			opponentLayer = LayerMask.NameToLayer("Player2");
			playerVars = GameObject.FindWithTag("Player1").GetComponent<PlayerVars>();
		}
		else // playerID == 2
		{
			unitLayer = LayerMask.NameToLayer("Player2");
			opponentLayer = LayerMask.NameToLayer("Player1");
			playerVars = GameObject.FindWithTag("Player2").GetComponent<PlayerVars>();
		}

		constants = GameObject.FindGameObjectWithTag("Constants").GetComponent<Constants>();
	}
	
	// Update is called once per frame
	void Update () {
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		if (Physics.Raycast(ray, out hit))
		{
			transform.position = new Vector3(hit.point.x, hit.point.y, hit.point.z);
		}
		EndDrag();
	}

	protected void EndDrag()
	{
		if (Input.GetMouseButtonDown (0)) {
			GameObject.FindGameObjectWithTag ("narrator").GetComponent<NarratorSpeech> ().casting ();
			FMODUnity.RuntimeManager.PlayOneShot (spellsound, transform.position);
			playerVars.calculateMagic(-constants.divineInterventionCost);
			GameObject.FindGameObjectWithTag ("stunAndHeal").GetComponent<SpellButtonCooldown> ().putOnCooldown (constants.divineInterventionCD);
			if (hit.transform != null) {
				// This is the bomb effect, detonating some distance up from target position
				PhotonNetwork.Instantiate("divineInterventionEffect", transform.position + new Vector3(0, 5f, 0), Quaternion.Euler(-90, 0, 0), 0);
                Collider[] hitColliders = Physics.OverlapSphere (transform.position, constants.spellRadius);
				foreach (Collider unit in hitColliders) {
					if (unit.gameObject.layer == unitLayer) { //friendly unit
						unit.gameObject.GetComponent<UnitAI>().heal(constants.divineInterventionHeal);
					} else if (unit.gameObject.layer == opponentLayer) { //enemy unit
						unit.gameObject.GetComponent<UnitAI>().stun(constants.divineInterventionStunDuration);
					}
				}
			}
			Destroy(gameObject);
		} else if (Input.GetMouseButtonDown(1)){
			Destroy(gameObject); 
		}
	}
}
