﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using RTS_Cam;
using UnityEngine.SceneManagement;

public class NetworkInGame : MonoBehaviour {

    [SerializeField] Camera sceneCamera;

    [SerializeField] Text player1Name;
    [SerializeField] Text player2Name;
    [SerializeField] Text player1Health;
    [SerializeField] Text player2Health;
    [SerializeField] Slider player1HealthSlider;
    [SerializeField] Slider player2HealthSlider;
    [SerializeField] Text magicText;
    [SerializeField] Slider magicSlider;
    [SerializeField] InputField messageWindow;
    [SerializeField] GameObject gameUI;
    [SerializeField] RTS_Camera cameraScript;
    [SerializeField] Transform[] cameraPositions;
    [SerializeField] GameObject errorWindow;
    [SerializeField] GameObject testButtons;

    public Constants constants;
    public GameObject textDisplayer;
    public GameObject redTower;
    public GameObject redTowerBase;
    public GameObject blueTower;
    public GameObject blueTowerBase;
    public const int messageCount = 6;
    public LayerMask ignoreLayer;

    private PhotonView photonView;
    private Queue<string> messages;

    public MonoBehaviour[] scriptsToResetOnRematch;
    public CooldownReset cdReset;

    public Texture2D cursorTexture;

    void Awake()
    {
        // Go back to main menu if we are not connected to Photon.
        if (!PhotonNetwork.connected)
        {
            SceneManager.LoadScene(0);
            return;
        }

        photonView = GetComponent<PhotonView>();
        messages = new Queue<string>(messageCount);

        Cursor.SetCursor(cursorTexture, Vector2.zero, CursorMode.ForceSoftware);
    }

    // Subscribe to the sceneloaded event when script is enabled, and unsub when it's disabled (happens when changing scenes)
    void OnEnable()
    {
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
    }

    void OnDisable()
    {
        SceneManager.sceneLoaded -= OnLevelFinishedLoading;
    }

    // This delegate pretty much replaces the Start() method
    void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        sceneCamera.transform.position = cameraPositions[0].position;
        sceneCamera.transform.rotation = cameraPositions[0].rotation;

        player1HealthSlider.maxValue = constants.towerStartHealth;
        player2HealthSlider.maxValue = constants.towerStartHealth;
        player1HealthSlider.value = constants.towerStartHealth;
        player2HealthSlider.value = constants.towerStartHealth;
        player1Health.text = constants.towerStartHealth.ToString();
        player2Health.text = constants.towerStartHealth.ToString();
        magicSlider.maxValue = constants.maxMagic;

        if (PhotonNetwork.isMasterClient)
        {
            //photonView.RPC("AddMessage", PhotonTargets.All, "<color=red>" + PhotonNetwork.player.name + "</color> <color=green>joined the game</color>");
            redTower.GetComponent<PhotonView>().TransferOwnership(PhotonNetwork.player);
            redTowerBase.GetComponent<PhotonView>().TransferOwnership(PhotonNetwork.player);
            blueTowerBase.layer = ignoreLayer;
            sceneCamera.transform.position = cameraPositions[1].position;
            sceneCamera.transform.rotation = cameraPositions[1].rotation;
        }

        else
        {
            //photonView.RPC("AddMessage", PhotonTargets.All, "<color=blue>" + PhotonNetwork.player.name + "</color> <color=green>joined the game</color>");
            blueTower.GetComponent<PhotonView>().TransferOwnership(PhotonNetwork.player);
            blueTowerBase.GetComponent<PhotonView>().TransferOwnership(PhotonNetwork.player);
            redTowerBase.layer = ignoreLayer;
            sceneCamera.transform.position = cameraPositions[2].position;
            sceneCamera.transform.rotation = cameraPositions[2].rotation;
        }

        sceneCamera.enabled = true;
        photonView.RPC("UpdatePlayerList_RPC", PhotonTargets.All, PhotonNetwork.player.ID, PhotonNetwork.player.name);
        photonView.RPC("UpdateHealthList_RPC", PhotonTargets.All, PhotonNetwork.player.ID, constants.towerStartHealth);
        UpdateMagicValue(constants.magicStartAmount);

        //textDisplayer.GetComponent<TextDisplayer>().prepareDisplay("<color=#F31E1E>Waiting for another player...</color>", 40, 0f);
        textDisplayer.GetComponent<PhotonView>().RPC("prepareDisplay", PhotonTargets.All, "<color=#F31E1E>Waiting for another player...</color>", 40, 0f);

        if (PhotonNetwork.isNonMasterClientInRoom)
        {
            photonView.RPC("StartGame", PhotonTargets.All);
        }

        if (PhotonNetwork.offlineMode == true)
        {
            constants.offlineMode = true;
            constants.playersToStart = 1;
            testButtons.SetActive(true);
            StartGame();
        }
    }

    //Display message and clear player fields on d/c
    private void OnPhotonPlayerDisconnected(PhotonPlayer player)
    {
        //photonView.RPC("AddMessage", PhotonTargets.AllBuffered, "<color=red>" + player.name + " left the game</color>");
        //photonView.RPC("UpdatePlayerList_RPC", PhotonTargets.All, PhotonNetwork.player.ID, "");
        //photonView.RPC("UpdateHealthList_RPC", PhotonTargets.All, PhotonNetwork.player.ID, null);
        gameUI.SetActive(false);
        cameraScript.enabled = false;
        errorWindow.SetActive(true);  
		GameObject.FindGameObjectWithTag ("narrator").GetComponent<NarratorSpeech> ().disconnected ();
    }

    void OnDisconnectedFromPhoton()
    {
        SceneManager.LoadScene(0);
    }

    public void Disconnect()
    {
        PhotonNetwork.LeaveRoom();       
    }

    private void OnLeftRoom()
    {
        SceneManager.LoadScene(0);
    }

    [PunRPC]
    void StartGame()
    {
        gameUI.SetActive(true);
        cameraScript.enabled = true;
        textDisplayer.GetComponent<PhotonView>().RPC("prepareDisplay", PhotonTargets.All, "<color=#63FF77FF>Fight!</color>", 80, 3f);
		GameObject.FindGameObjectWithTag ("narrator").GetComponent<NarratorSpeech> ().gameStart ();
    }

    [PunRPC]
    void AddMessage(string message)
    {
        messages.Enqueue(message);

        if (messages.Count > messageCount)
        {
            messages.Dequeue();
        }

        messageWindow.text = "";

        foreach (string m in messages)
        {
            messageWindow.text += m + "\n";
        }      
    }

    [PunRPC]
    void UpdatePlayerList_RPC(int playerID, string name)
    {
        if (playerID == 1)
        {
            player1Name.text = name;
        }
        else //playerID == 2
        {
            player2Name.text = name;
        }
    }

    [PunRPC]
    void UpdateHealthList_RPC(int playerID, float health)
    {
        if (playerID == 1)
        {
            player1Health.text = health.ToString();
            player1HealthSlider.value = health;
        }
        else //playerID == 2
        {
            player2Health.text = health.ToString();
            player2HealthSlider.value = health;
        }
    }

    public void UpdateMagicValue(float magic)
    {
		magicText.text = magic.ToString();
        magicSlider.value = magic;
    }

    [PunRPC]
    public void EndGame(int playerID)
    {
        string msg;

        if (playerID == PhotonNetwork.player.ID)
        {
            msg = "<color=#F31E1E>You lose!\n <size=25>Press F1 to play again or Esc to quit.</size></color>";
			GameObject.FindGameObjectWithTag ("narrator").GetComponent<NarratorSpeech> ().gameOver (false);
        }

        else
        {
            msg = "<color=#63FF77FF>You win!\n <size=25>Press F1 to play again or Esc to quit.</size></color>";
			GameObject.FindGameObjectWithTag ("narrator").GetComponent<NarratorSpeech> ().gameOver (true);
        }
        gameUI.SetActive(false);
        textDisplayer.GetComponent<TextDisplayer>().prepareDisplay(msg, 40, 0f);
    }

    [PunRPC]
    public void Rematch()
    {
        // The scripts in the array need to have the desired things to reset in OnEnable() function
        // Do not put UI scripts here because the UI can be toggled on/off by the player
        foreach (MonoBehaviour script in scriptsToResetOnRematch)
        {
            script.enabled = false;
            script.enabled = true;
        }

        cdReset.ResetCooldown();

        redTowerBase.SetActive(true);
        redTowerBase.GetComponent<CapsuleCollider>().enabled = true;
        blueTowerBase.SetActive(true);
        blueTowerBase.GetComponent<CapsuleCollider>().enabled = true;
        PhotonNetwork.DestroyPlayerObjects(PhotonNetwork.player);
        OnLevelFinishedLoading(SceneManager.GetActiveScene(), LoadSceneMode.Single);

        //if (PhotonNetwork.isMasterClient)
        //{
        //    foreach (PhotonPlayer player in PhotonNetwork.playerList)
        //    {
        //        PhotonNetwork.DestroyPlayerObjects(player);
        //        PhotonNetwork.LoadLevel(1);
        //    }
        //}
    }
}
