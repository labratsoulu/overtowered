﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class NoMagicText : MonoBehaviour {

	private float disappearTimer;
	private float timerTime = 1f;

	// Update is called once per frame
	void Update () {
		if (disappearTimer > 0)
			disappearTimer -= Time.deltaTime;
		else
			gameObject.SetActive (false);
	}

	public void showText(){
		disappearTimer = timerTime;
	}
}
