﻿using UnityEngine;
using System.Collections;

public class BlockadeToggle : MonoBehaviour {

    public Constants constants;
    public PlayerVars player1Vars;
    public PlayerVars player2Vars;
    public GameObject blockade;
    public float toggleCost = 50f;

    private PhotonView thisView;
    private PlayerVars playerVars;

    void Start()
    {
        thisView = gameObject.GetComponent<PhotonView>();

        if (PhotonNetwork.player.ID == 1)
        {
            playerVars = player1Vars;
        }
        
        else // playerID == 2
        {
            playerVars = player2Vars;
        }
    }

    public void Toggle()
    {
        if (playerVars.magic >= toggleCost)
        {
            playerVars.calculateMagic(-toggleCost);
            thisView.RPC("Toggle_RPC", PhotonTargets.All);
        }       
    }

    [PunRPC]
    private void Toggle_RPC()
    {
        blockade.SetActive(!blockade.activeInHierarchy);
    }
}
