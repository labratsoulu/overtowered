﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TowerPieceUI : MonoBehaviour {

	public TowerPieceUI up;
	public TowerPieceUI down;
	public TowerPieceUI left;
	public TowerPieceUI right;

	public TowerBaseSpawner redTowerBase;
	public TowerBaseSpawner blueTowerBase;
	public GameObject redPiece;
	public GameObject bluePiece;

	public UIPieceState state;
	public Sprite activatedSprite;
	public Sprite builtSprite;

	public GameObject radialMenu;

	public PlayerVars player1Vars;
	public PlayerVars player2Vars;
	private PlayerVars totalMagic;
	public GameObject noMagicText;

	public Constants constants;

	private Button button;
	private TowerType.BlockType builtType = TowerType.BlockType.Empty;
	private float spawnTimer = 0;
	private float spawnTime = 10;
	private bool active = true;

	public enum UIPieceState
	{
		locked,
		activated,
		built
	};

	// Use this for initialization
	void Awake () {
		button = GetComponent<Button> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (state == UIPieceState.locked && ((up != null && up.state == UIPieceState.built) || (down != null && down.state == UIPieceState.built) || (left != null && left.state == UIPieceState.built) || (right != null && right.state == UIPieceState.built))) {
			state = UIPieceState.activated;
			button.enabled = true;
			GetComponent<Image> ().sprite = activatedSprite;
		} else if(builtType != TowerType.BlockType.Empty) {
			spawnTimer += Time.deltaTime;
			float barSize;
			if ((spawnTimer / spawnTime) * 100 >= 100) 
				barSize = 100;
			else 
				barSize = (spawnTimer / spawnTime) * 100;
			transform.GetChild (0).GetComponent<Slider> ().value = barSize;
			if (spawnTimer >= spawnTime && active) {
				TowerBaseSpawner towerBase;
				//get relevant basepiece
				if (PhotonNetwork.player.ID == 1) {
					towerBase = redTowerBase;
				} else { // ID == 2
					towerBase = blueTowerBase;
				}
				towerBase.spawnUnitByType (builtType);
				spawnTimer = 0;
			}
		}

		//TODO check adjacency when state = built
	}

	public void pressed(){

		if (state == UIPieceState.activated) {
			
			GameObject temp = Instantiate (radialMenu);
			temp.transform.SetParent (transform.parent);
			temp.transform.position = transform.position;
			temp.GetComponent<RMenu> ().UIPiece = gameObject;

		} else if (state == UIPieceState.built) {
			//activate or deactivate piece
			if (active) {
				active = false;
				GetComponent<Image> ().color = new Color (0.7f, 0.7f, 0.7f, 1f);
			} else {
				active = true;
				GetComponent<Image> ().color = new Color (1f, 1f, 1f, 1f);
			}
		}
	}

	public void buildPiece(TowerType.BlockType type){
		if (PhotonNetwork.player.ID == 1)
		{
			totalMagic = player1Vars;
		}

		else // ID == 2
		{
			totalMagic = player2Vars;
		}
		if (totalMagic.magic >= 25) {

			totalMagic.calculateMagic (-25);

			state = UIPieceState.built;

			GameObject buildPiece; 
			TowerBaseSpawner towerBase;

			//get relevant basepiece
			if (PhotonNetwork.player.ID == 1) {
				buildPiece = redPiece;
				towerBase = redTowerBase;
			} else { // ID == 2
				buildPiece = bluePiece;
				towerBase = blueTowerBase;
			}

			buildPiece.GetComponent<PhotonView> ().RPC ("showPiece", PhotonTargets.All);

			GetComponent<Image> ().sprite = builtSprite;

			//towerBase.addTowerPiece (type, TowerType.BlockType.Empty);
			builtType = type;
			transform.GetChild (0).gameObject.SetActive (true);
		} else {
			noMagicText.SetActive (true);
			noMagicText.GetComponent<NoMagicText> ().showText ();
			GameObject.FindGameObjectWithTag ("narrator").GetComponent<NarratorSpeech> ().noMagic ();
		}
	}
}
