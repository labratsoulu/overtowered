﻿using UnityEngine;
using System.Collections;

public class RMenu : MonoBehaviour {

	public GameObject button1;
	public GameObject button2;
	public GameObject button3;
	public GameObject button4;
	public GameObject button5;
	public GameObject button6;

	[HideInInspector]
	public GameObject UIPiece;

	private Vector3 button1Pos = new Vector3(30,60,0);
	private Vector3 button2Pos = new Vector3(60,0,0);
	private Vector3 button3Pos = new Vector3(30,-60,0);
	private Vector3 button4Pos = new Vector3(-30,-60,0);
	private Vector3 button5Pos = new Vector3(-60,0,0);
	private Vector3 button6Pos = new Vector3(-30,60,0);
	private float speed = 800f;
	private float timeElapsed;

	private bool colapse;
	private float timeElapsed2;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (timeElapsed < 0.2f) {
			timeElapsed += Time.deltaTime;
			button1.GetComponent<RectTransform> ().anchoredPosition = Vector3.Lerp (Vector3.zero, button1Pos, (speed * timeElapsed) / Vector3.Distance (Vector3.zero, button1Pos));
			button2.GetComponent<RectTransform> ().anchoredPosition = Vector3.Lerp (Vector3.zero, button2Pos, (speed * timeElapsed) / Vector3.Distance (Vector3.zero, button2Pos));
			button3.GetComponent<RectTransform> ().anchoredPosition = Vector3.Lerp (Vector3.zero, button3Pos, (speed * timeElapsed) / Vector3.Distance (Vector3.zero, button3Pos));
			button4.GetComponent<RectTransform> ().anchoredPosition = Vector3.Lerp (Vector3.zero, button4Pos, (speed * timeElapsed) / Vector3.Distance (Vector3.zero, button4Pos));
			button5.GetComponent<RectTransform> ().anchoredPosition = Vector3.Lerp (Vector3.zero, button5Pos, (speed * timeElapsed) / Vector3.Distance (Vector3.zero, button5Pos));
			button6.GetComponent<RectTransform> ().anchoredPosition = Vector3.Lerp (Vector3.zero, button6Pos, (speed * timeElapsed) / Vector3.Distance (Vector3.zero, button6Pos));
		}

		if (Input.GetMouseButtonUp (0)) {
			colapse = true;
			timeElapsed = 1;
		}

		if (colapse) {
			timeElapsed2 += Time.deltaTime;
			button1.GetComponent<RectTransform> ().anchoredPosition = Vector3.Lerp (button1Pos, Vector3.zero, (speed * timeElapsed2) / Vector3.Distance (button1Pos, Vector3.zero));
			button2.GetComponent<RectTransform> ().anchoredPosition = Vector3.Lerp (button2Pos, Vector3.zero, (speed * timeElapsed2) / Vector3.Distance (button2Pos, Vector3.zero));
			button3.GetComponent<RectTransform> ().anchoredPosition = Vector3.Lerp (button3Pos, Vector3.zero, (speed * timeElapsed2) / Vector3.Distance (button3Pos, Vector3.zero));
			button4.GetComponent<RectTransform> ().anchoredPosition = Vector3.Lerp (button4Pos, Vector3.zero, (speed * timeElapsed2) / Vector3.Distance (button4Pos, Vector3.zero));
			button5.GetComponent<RectTransform> ().anchoredPosition = Vector3.Lerp (button5Pos, Vector3.zero, (speed * timeElapsed2) / Vector3.Distance (button5Pos, Vector3.zero));
			button6.GetComponent<RectTransform> ().anchoredPosition = Vector3.Lerp (button6Pos, Vector3.zero, (speed * timeElapsed2) / Vector3.Distance (button6Pos, Vector3.zero));
			if (timeElapsed2 > 0.1f)
				Destroy (gameObject);
		}
	}

	public void build(string type){
		TowerType.BlockType temp = (TowerType.BlockType) System.Enum.Parse (typeof(TowerType.BlockType), type);
		UIPiece.GetComponent<TowerPieceUI> ().buildPiece (temp);
	}
}
