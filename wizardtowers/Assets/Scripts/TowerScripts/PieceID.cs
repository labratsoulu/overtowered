﻿using UnityEngine;
using System.Collections;

public class PieceID : MonoBehaviour {

	public static int lastID = 0;
	public int ID;

	void Awake () {
		lastID++;
		ID = lastID;
	}
}
