﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TowerBaseSpawner : MonoBehaviour {

	public GameObject golem;
	public GameObject apprentice;
	public GameObject skeleton;

    public Constants constants;
    public PlayerVars playerVars;

    private float spawnRadius;
    private float spawnRate;

	//the first float of each list holds the countdown to the next spawn. The second refers to the powerup.
	private List<List<float>> golemSpawns; 
	private List<List<float>> apprenticeSpawns; 
	private List<List<float>> skeletonSpawns;

    void OnEnable()
    {
        spawnRadius = constants.unitSpawnRadius;
        spawnRate = constants.unitSpawnRate;

		golemSpawns = new List<List<float>> ();
		apprenticeSpawns = new List<List<float>> ();
		skeletonSpawns = new List<List<float>> ();
    }

	// Update is called once per frame
	void Update () {
		//GameObject unit;
		foreach (List<float> piece in golemSpawns) {
			piece [0] += Time.deltaTime;
			if (piece [0] >= spawnRate) {
				piece [0] = 0f;
                spawnUnit("golem");
				//upgradeUnit (unit, piece [1]);
			}
		}
		foreach (List<float> piece in apprenticeSpawns) {
			piece [0] += Time.deltaTime;
			if (piece [0] >= spawnRate) {
				piece [0] = 0f;
                spawnUnit("apprentice");
                //upgradeUnit (unit, piece [1]);
            }
		}
		foreach (List<float> piece in skeletonSpawns) {
			piece [0] += Time.deltaTime;
			if (piece [0] >= spawnRate) {
				piece [0] = 0f;
                spawnUnit("skeleton");
                //upgradeUnit (unit, piece [1]);
            }
		}
//		golemSpawn += golemPieces * Time.deltaTime;
//		if (golemSpawn >= spawnRate) {
//			golemSpawn = 0f;
//			PhotonNetwork.Instantiate(golem.name, new Vector3(transform.position.x + Random.Range(-spawnRadius, spawnRadius), transform.position.y, transform.position.z + Random.Range(-spawnRadius, spawnRadius)), transform.rotation, 0);
//		}
//
//		apprenticeSpawn += apprenticePieces * Time.deltaTime;
//		if (apprenticeSpawn >= spawnRate) {
//			apprenticeSpawn = 0f;
//			PhotonNetwork.Instantiate(apprentice.name, new Vector3(transform.position.x + Random.Range(-spawnRadius, spawnRadius), transform.position.y, transform.position.z + Random.Range(-spawnRadius, spawnRadius)), transform.rotation, 0);
//		}
//
//		skeletonSpawn += skeletonPieces * Time.deltaTime;
//		if (skeletonSpawn >= spawnRate) {
//			skeletonSpawn = 0f;
//			PhotonNetwork.Instantiate(skeleton.name, new Vector3(transform.position.x + Random.Range(-spawnRadius, spawnRadius), transform.position.y, transform.position.z + Random.Range(-spawnRadius, spawnRadius)), transform.rotation, 0);
//		}
	}

	private void upgradeUnit(GameObject unit, float upgrade){
		Debug.Log (upgrade);
		if (upgrade > 3) {
			
			switch ((int)upgrade) {
			case 4://+25 damage
				unit.GetComponent<UnitAI>().damage += 25;
				break;

			case 5://+25 health
				unit.GetComponent<UnitAI>().health += 25;
				break;

			case 6://+0.5 range
				unit.GetComponent<UnitAI>().attackDistance += 5f;
				break;

			case 7://+0.5 movement speed. Won't be implemented for now
				break;

			case 8://-0.5 cooldown
				unit.GetComponent<UnitAI>().attackInterval -= 0.5f;
				break;

			case 9://explode on death. Won't be implemented for now
				break;
				
			}
		}
	}

	public void addTowerPiece(TowerType.BlockType type, TowerType.BlockType upgrade){
//		switch (type) {
//		case TowerType.BlockType.ApprenticeSpawn:
//			apprenticePieces++;
//			break;
//		case TowerType.BlockType.GolemSpawn:
//			golemPieces++;
//			break;
//		case TowerType.BlockType.SkeletonSpawn:
//			skeletonPieces++;
//			break;
//		}
		List<float> temp = new List<float>();
		temp.Add (0f);
		float upgradeNumber = (float)(int)upgrade;
		temp.Add (upgradeNumber);
		switch (type) {
		case TowerType.BlockType.ApprenticeSpawn:
			apprenticeSpawns.Add (temp);
			break;
		case TowerType.BlockType.GolemSpawn:
			golemSpawns.Add (temp);
			break;
		case TowerType.BlockType.SkeletonSpawn:
			skeletonSpawns.Add (temp);
			break;
		}
	}

	public void spawnUnit(string unitName){

        if (playerVars.unitAmount < constants.maxUnitAmount)
        {
            switch (unitName)
            {
                case "apprentice":
                    PhotonNetwork.Instantiate(apprentice.name, new Vector3(transform.position.x + Random.Range(-spawnRadius, spawnRadius), transform.position.y, transform.position.z + Random.Range(-spawnRadius, spawnRadius)), transform.rotation, 0);
                    break;
                case "skeleton":
                    PhotonNetwork.Instantiate(skeleton.name, new Vector3(transform.position.x + Random.Range(-spawnRadius, spawnRadius), transform.position.y, transform.position.z + Random.Range(-spawnRadius, spawnRadius)), transform.rotation, 0);
                    break;
                case "golem":
                    PhotonNetwork.Instantiate(golem.name, new Vector3(transform.position.x + Random.Range(-spawnRadius, spawnRadius), transform.position.y, transform.position.z + Random.Range(-spawnRadius, spawnRadius)), transform.rotation, 0);
                    break;
            }
            playerVars.changeUnitAmount(1);
        }

	}

	public void spawnUnitByType(TowerType.BlockType type){

		if (playerVars.unitAmount < constants.maxUnitAmount)
		{
			switch (type)
			{
			case TowerType.BlockType.ApprenticeSpawn:
				PhotonNetwork.Instantiate(apprentice.name, new Vector3(transform.position.x + Random.Range(-spawnRadius, spawnRadius), transform.position.y, transform.position.z + Random.Range(-spawnRadius, spawnRadius)), transform.rotation, 0);
				break;
			case TowerType.BlockType.SkeletonSpawn:
				PhotonNetwork.Instantiate(skeleton.name, new Vector3(transform.position.x + Random.Range(-spawnRadius, spawnRadius), transform.position.y, transform.position.z + Random.Range(-spawnRadius, spawnRadius)), transform.rotation, 0);
				break;
			case TowerType.BlockType.GolemSpawn:
				PhotonNetwork.Instantiate(golem.name, new Vector3(transform.position.x + Random.Range(-spawnRadius, spawnRadius), transform.position.y, transform.position.z + Random.Range(-spawnRadius, spawnRadius)), transform.rotation, 0);
				break;
			}
			playerVars.changeUnitAmount(1);
		}

	}
}