﻿using UnityEngine;
using System.Collections;

public class TowerType : MonoBehaviour {

	public BlockType Type;

	public enum BlockType {
		Empty, 
		GolemSpawn, 
		SkeletonSpawn, 
		ApprenticeSpawn, 
		DamageUpgrade, 
		HealthUpgrade, 
		RangeUpgrade, 
		SpeedUpgrade, 
		CooldownUpgrade,
		VolatileUpgrade,
		GatheringPiece
	};
}
