﻿using UnityEngine;
using System.Collections;

public class PlacedMainPiece : MonoBehaviour {

	public LayerMask ignoreLayer;

    private Constants constants;

    private GameObject player1Vars;
    private GameObject player2Vars;

    private PhotonView thisView;

    void Awake()
    {
        constants = GameObject.FindWithTag("Constants").GetComponent<Constants>();
        player1Vars = GameObject.FindWithTag("Player1");
        player2Vars = GameObject.FindWithTag("Player2");
        thisView = GetComponent<PhotonView>();

        if (thisView.isMine)
        {
//            if (thisView.owner.ID == 1)
//            {
//                player1Vars.GetComponent<PlayerVars>().calculateMagic(-constants.pieceMagicCost);
//            }
//            else // ID == 2
//            {
//                player2Vars.GetComponent<PlayerVars>().calculateMagic(-constants.pieceMagicCost);
//            }
        }

    }

    [PunRPC]
	public void setAllParents(int parentID){
		gameObject.layer = ignoreLayer;
		GameObject towerBase;
		if (PhotonNetwork.player.ID == 1) {
			towerBase = GameObject.Find("Tower Base (Blue)");
		} else {
			towerBase = GameObject.Find("Tower Base (Red)");
		}
		if (parentID == 0) {
			transform.SetParent (towerBase.transform.GetChild (0));
		} else {
			Debug.Log (parentID + "    " + towerBase);
			GameObject correctPiece = findCorrectPiece (parentID, towerBase);
			if (correctPiece.tag == "towerblock")
				transform.SetParent (correctPiece.transform);
			else
				transform.SetParent(correctPiece.transform.GetChild(0));
		}
	}

	public GameObject findCorrectPiece(int IDNumber, GameObject towerBase){
		foreach (Transform child in towerBase.transform) {
			if (child.gameObject.GetComponent<PieceID> () != null && child.gameObject.GetComponent<PieceID> ().ID == IDNumber)
				return child.gameObject;
			else {
				GameObject result;
				result = findCorrectPiece (IDNumber, child.gameObject);
				if (result != null)
					return result;
			}
		}

		return null;
	}

	[PunRPC]
	public void showPiece(){
		transform.GetChild (0).GetComponent<MeshRenderer> ().enabled = true;
		//transform.GetChild (1).GetComponent<MeshRenderer> ().enabled = true;
	}
}
