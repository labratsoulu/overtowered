﻿using UnityEngine;
using System.Collections;

public class SimpleMove : MonoBehaviour {

	public float speed = 10;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		
		Vector3 pos = transform.position;
		if (Input.GetKey (KeyCode.LeftArrow)) {
			pos.x += Time.deltaTime * speed;
		} if (Input.GetKey (KeyCode.RightArrow)) {
			pos.x -= Time.deltaTime * speed;
		} if (Input.GetKey (KeyCode.UpArrow)) {
			pos.z += Time.deltaTime * speed;
		} if (Input.GetKey (KeyCode.DownArrow)) {
			pos.z -= Time.deltaTime * speed;
		} if (Input.GetKey (KeyCode.Return)) {
			pos.y += Time.deltaTime * speed;
		} if (Input.GetKey (KeyCode.RightShift)) {
			pos.y -= Time.deltaTime * speed;
		}
		transform.position = pos;
	}
}
