﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SpellButtonCooldown : MonoBehaviour {

	public float timeLeft;

	private Button button;
	private Text text;

	void Start()
    {
        timeLeft = 0f;
		button = GetComponent<Button>();
		text = transform.GetChild (0).gameObject.GetComponent<Text> ();
	}

	void Update()
    {
		timeLeft -= Time.deltaTime;
		if (timeLeft > 0) {
			text.text = "" + ((int)timeLeft + 1);
		} else {
			button.enabled = true;
			text.text = "";
		}
	}

	public void putOnCooldown(float seconds)
    {
		//make button unavailable unil timer runs out.
		//also make it visually clear how much longer.
		button.enabled = false;
		timeLeft = seconds;
		text.text = "" + seconds;
	}
}
