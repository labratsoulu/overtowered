﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CooldownReset : MonoBehaviour {

    public SpellButtonCooldown[] spells;
    public Button arcaneBombButton;

	public void ResetCooldown()
    {
	    foreach (SpellButtonCooldown spell in spells)
        {
            spell.timeLeft = 0;
        }

        arcaneBombButton.interactable = true;
    }
}
