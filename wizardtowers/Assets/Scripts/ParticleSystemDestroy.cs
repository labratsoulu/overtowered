﻿using UnityEngine;
using System.Collections;

public class ParticleSystemDestroy : MonoBehaviour {

    private IEnumerator Start()
    {
        // Wait for the particle system to finish
        yield return new WaitForSeconds(GetComponent<ParticleSystem>().duration);

        // A networked object is destroyed by its owner
        if (GetComponent<PhotonView>() != null)
        {
            if (GetComponent<PhotonView>().isMine)
            {
                PhotonNetwork.Destroy(gameObject);
            }
        }

        // A normal object is destroyed normally
        else
        {
            Destroy(gameObject);
        }         
    }
}
