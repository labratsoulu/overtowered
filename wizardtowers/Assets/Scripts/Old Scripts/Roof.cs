﻿using UnityEngine;
using System.Collections;

public class Roof : MonoBehaviour {

	public void setPosition(int parentID){
		gameObject.GetComponent<PhotonView> ().RPC ("setAllParents", PhotonTargets.Others, parentID);
	}

	[PunRPC]
	public void setAllParents(int parentID){
		GameObject towerBase;
		if (PhotonNetwork.player.ID == 1) {
			towerBase = GameObject.Find("Tower Base (Blue)");
		} else {
			towerBase = GameObject.Find ("Tower Base (Red)");
		}

		GameObject correctPiece = findCorrectPiece (parentID, towerBase);

		transform.SetParent(correctPiece.transform);
		transform.localPosition = new Vector3 (0, 4, 0);
	}

	public GameObject findCorrectPiece(int IDNumber, GameObject towerBase){
		foreach (Transform child in towerBase.transform) {
			if (child.gameObject.GetComponent<PieceID> () != null && child.gameObject.GetComponent<PieceID> ().ID == IDNumber)
				return child.gameObject;
			else {
				GameObject result;
				result = findCorrectPiece (IDNumber, child.gameObject);
				if (result != null)
					return result;
			}
		}

		return null;
	}
}
