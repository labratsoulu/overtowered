﻿using UnityEngine;
using System.Collections;

public class MainPiece : AbstractTowerPiece {

	public GameObject roof;
	public GameObject Piece;

	private float modelHeight = 4f;
	private bool mouseClick;

	void Awake(){
		
	}

	protected override void Dragged(){
		base.Dragged ();
		if (hit.transform != null) {
			if (hit.transform.tag == "towerblock") {
				//posY = hit.transform.position.y + hit.transform.localScale.y;
				//posY += transform.localScale.y;
				posY = modelHeight;
				transform.position = new Vector3 (hit.transform.position.x, (hit.transform.position.y + posY), hit.transform.position.z);
			} else if (hit.transform.tag == "towerbase") {
				posY = hit.transform.position.y + hit.transform.localScale.y;
				//posY += transform.localScale.y;
				transform.position = new Vector3 (hit.transform.position.x, posY, hit.transform.position.z);
			} else if (hit.transform.tag == "connector") {
				posY = hit.transform.GetChild (0).position.y + hit.transform.GetChild (0).localScale.y;
				posY += transform.localScale.y;
				transform.position = new Vector3 (hit.transform.GetChild (0).position.x, posY, hit.transform.GetChild (0).position.z);
			}
		}

		if (Input.GetMouseButtonUp(0))
		{
			mouseClick = false;
		}
		if (mouseClick==false)
		{
			EndDrag();
		}
	}

	private TowerType.BlockType getUpgrade(Transform piece){
		if (piece != null) {
			TowerType component = piece.gameObject.GetComponent<TowerType> ();
			if (component != null && (int)component.Type > 3) {
				return component.Type;
			} else {
				return getUpgrade (piece.parent);
			}
		} else {
			return TowerType.BlockType.Empty;
		}
	}

	protected override void EndDrag()
	{
		if (Input.GetMouseButtonDown (0) && state == blockstate.dragged) {
			Quaternion rotation = transform.rotation;
			rotation *= Quaternion.Euler (0, (Random.Range(0,50)-25), 0);
			if (hit.transform != null && hit.transform.tag == "towerblock") {
				GameObject piece;
				piece = PhotonNetwork.Instantiate (Piece.name, transform.position, rotation, 0);
				piece.GetComponent<PhotonView> ().RPC ("setAllParents", PhotonTargets.Others, hit.transform.gameObject.GetComponent<PieceID> ().ID);
				piece.transform.root.SetParent (hit.transform);
				hit.transform.gameObject.GetComponent<CapsuleCollider> ().enabled = false;
				base.EndDrag ();

				piece.GetComponent<TowerType>().Type = GetComponent<TowerType>().Type;
				GameObject gatheringPiece = getGatheringPiece (piece.transform);
				if (gatheringPiece == null)
					piece.transform.root.gameObject.GetComponent<TowerBaseSpawner> ().addTowerPiece (GetComponent<TowerType> ().Type, getUpgrade (piece.transform.parent));
				else
					gatheringPiece.GetComponent<GatheringPiece> ().addTowerPiece (GetComponent<TowerType> ().Type);
				
				GameObject.FindGameObjectWithTag ("MainCamera").GetComponent<TowerSounds> ().placeSound ();

				foreach (Transform child in hit.transform) {
					
					if (child.tag == "Roof") {
						//Make sure the roof piece stays on top
						Transform roof = child;
						roof.gameObject.GetComponent<Roof> ().setPosition (piece.GetComponent<PieceID> ().ID);
						roof.SetParent (piece.transform);
						Vector3 newRoofPos = roof.localPosition;
						//newRoofPos.y = piece.transform.position.y + piece.transform.root.localScale.y;
						//newRoofPos.y += roof.localScale.y;
						newRoofPos.y = modelHeight;
						roof.localPosition = newRoofPos;
						break;
					}
				}
			} else if (hit.transform != null && (hit.transform.tag == "connector" || hit.transform.tag == "towerbase")) {
				GameObject piece;
				piece = PhotonNetwork.Instantiate (Piece.name, transform.position, rotation, 0);
				piece.transform.root.SetParent (hit.transform.GetChild (0));
				if (hit.transform.gameObject.GetComponent<PlacedConnectorPiece> () != null) {
					piece.gameObject.GetComponent<PhotonView> ().RPC ("setAllParents", PhotonTargets.Others, hit.transform.gameObject.GetComponent<PieceID> ().ID);
				} else {
					piece.gameObject.GetComponent<PhotonView> ().RPC ("setAllParents", PhotonTargets.Others, 0);
				}
                
				hit.transform.gameObject.GetComponent<CapsuleCollider> ().enabled = false;
				base.EndDrag ();

				//make sure the tower will start spawning the units and that the correct upgrade is applied
				piece.GetComponent<TowerType>().Type = GetComponent<TowerType>().Type;
				GameObject gatheringPiece = getGatheringPiece (piece.transform);
				if (gatheringPiece == null)
					piece.transform.root.gameObject.GetComponent<TowerBaseSpawner> ().addTowerPiece (GetComponent<TowerType> ().Type, getUpgrade (piece.transform.parent));
				else
					gatheringPiece.GetComponent<GatheringPiece> ().addTowerPiece (GetComponent<TowerType> ().Type);

				GameObject.FindGameObjectWithTag ("MainCamera").GetComponent<TowerSounds> ().placeSound ();


				Debug.Log(piece.GetComponent<PieceID> ().ID);
				//add roof
				GameObject temp = PhotonNetwork.Instantiate (roof.name, transform.position, transform.rotation, 0);
				temp.transform.SetParent (piece.transform);

				temp.gameObject.GetComponent<Roof> ().setPosition (piece.GetComponent<PieceID> ().ID);
				temp.transform.localPosition = new Vector3 (0f, 0f, 0f);
				Vector3 newRoofPos = temp.transform.localPosition;
				//newRoofPos.y = piece.transform.position.y + piece.transform.root.localScale.y;
				//newRoofPos.y += temp.transform.localScale.y;
				newRoofPos.y = modelHeight;
				temp.transform.localPosition = newRoofPos;
			}
			Destroy (gameObject);
		} else if (Input.GetMouseButtonDown (1)) {
			Destroy (gameObject);
		}
	}

	private GameObject getGatheringPiece(Transform piece){
		if (piece.parent != null) {
			if (piece.parent.tag == "connector") {
				return piece.parent.gameObject;
			} else {
				return getGatheringPiece (piece.parent);
			}
		} else
			return null;
	}
}
