﻿using UnityEngine;
using System.Collections;

public class Float : MonoBehaviour
{

    public float waterLevel = 4f;
    public float floatHeight = 2f;
    public float bounceDamp = 0.05f;
    public Vector3 buoyancyCentreOffset;

    private float forceFactor;
    private Vector3 actionPoint;
    private Vector3 upLift;
    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {

        actionPoint = transform.position + transform.TransformDirection(buoyancyCentreOffset);
        forceFactor = 1f - ((actionPoint.y - waterLevel) / floatHeight);

        if (forceFactor > 0f)
        {
            upLift = -Physics.gravity * (forceFactor - rb.velocity.y * bounceDamp);
            rb.AddForceAtPosition(upLift, actionPoint);
        }

    }
}