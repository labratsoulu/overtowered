﻿using UnityEngine;
using System.Collections;

public class cannonLookAt : MonoBehaviour {

	public Transform target;

	void Update()
    {
		transform.LookAt(target, Vector3.up);
	}
}
